import 'package:flutter/material.dart';

class DrawerPage extends StatelessWidget {
  const DrawerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: ListView(
          children: const [
            DrawerHeader(
                child:UserAccountsDrawerHeader(
                  accountName: Text("Sudhir Kumar"),
                  accountEmail:Text("sudhirkumar.edugaon@gmail.com"),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: AssetImage("assets/images/sudhir.jpeg"),
                  ),

                )),
            ListTile(
              leading: Icon(Icons.home),
              title: Text("Home"),
            ),
                  ListTile(
                  leading: Icon(Icons.share),
              title: Text("Share"),
            ),
            ListTile(
              leading: Icon(Icons.edit),
              title: Text("Edit"),
            ),
            ListTile(
              leading: Icon(Icons.email),
              title: Text("Email"),
            ),
            ListTile(
              leading: Icon(Icons.lock),
              title: Text("Password"),
            ),
            ListTile(
              leading: Icon(Icons.perm_contact_calendar),
              title: Text("Person"),
            ),
            ListTile(
              leading: Icon(Icons.delete),
              title: Text("Delete"),
            ),
          ],
        ),
      ),
    );
  }
}
