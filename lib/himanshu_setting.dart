import 'package:flutter/material.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Settings'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            ListTile(
              title: Text('Notification Settings'),
              subtitle: Text('Configure notification preferences'),
              leading: Icon(Icons.notifications),
              onTap: () {
                // Add navigation to the notification settings page.
              },
            ),
            Divider(),
            ListTile(
              title: Text('Account Settings'),
              subtitle: Text('Manage your account settings'),
              leading: Icon(Icons.account_circle),
              onTap: () {
                // Add navigation to the account settings page.
              },
            ),
            Divider(),
            ListTile(
              title: Text('Appearance Settings'),
              subtitle: Text('Customize the app appearance'),
              leading: Icon(Icons.color_lens),
              onTap: () {
                // Add navigation to the appearance settings page.
              },
            ),
            Divider(),
            ListTile(
              title: Text('Your Password'),
              subtitle: Text('Reset && View Password'),
              leading: Icon(Icons.password_outlined),
              onTap: (){

              },
            )
          ],
        ),
      ),
    );
  }
}
